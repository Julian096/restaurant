import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: () => ({
      products:[
        {
          photo: require("@/assets/img/enchiladas.jpg"),
          name: "Enchiladas huastecas",
          description: "Orden de 5 tortillas bañadas en salsa verde acompañadas de verdura, cecina y frijoles refritos.",
          count: 1,
          price: 70,
          total: 70,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/zacahuil.jpg"),
          name: "Zacahuil",
          description: "Platillo estilo tamal revuelto con diferentes chiles y carne de cerdo o pollo para dar color y sabor, se acompaña con limon y chiles en vinagre.",
          count: 1,
          price: 40,
          total: 40,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/gorditas.jpeg"),
          name: "Gorditas",
          description: "Orden de 4 pzs. Tortillas infladas y rellenas de diferentes guisos.",
          count: 1,
          price: 35,
          total: 35,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/bocoles.jpg"),
          name: "Bocoles",
          description: "Orden de 4 pzs. Platillo a base de maza y manteca de puerco, se abren por la mitad y se rellenan de diferentes guisos.",
          count: 1,
          price: 40,
          total: 40,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/migada.jpg"),
          name: "Migada",
          description: "Tortilla gruesa grande pellizcada cubierta con diferentes guisos y verdura.",
          count: 1,
          price: 50,
          total: 50,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/sopes.jpg"),
          name: "Sopes",
          description: "Orden de 4 pzs. Tortilla gruesa pequeña pellizcada cubierta con diferentes guisos y verdura.",
          count: 1,
          price: 40,
          total: 40,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/menudo.jpg"),
          name: "Menudo",
          description: "Panzita y patitas de res en caldo con chile rojo.",
          count: 1,
          price: 70,
          total: 70,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/pozole.jpg"),
          name: "Pozole",
          description: "Carne de puerco con maiz en caldo y guisado con chile rojo.",
          count: 1,
          price: 60,
          total: 60,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/rojos.jpg"),
          name: "Tacos rojos",
          description: "Orden de 4 pzs. Tortillas fritas en salsa roja y rellenas de carne, papa o frijoles. Se acompaña con lechuga, tomate, queso y chiles en vinagre.",
          count: 1,
          price: 35,
          total: 35,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/cafe.jpeg"),
          name: "Café",
          description: "Café de olla preparado con canela.",
          count: 1,
          price: 12,
          total: 12,
          category: "Bebida"
        },
        {
          photo: require("@/assets/img/refresco.jpg"),
          name: "Refresco",
          description: "Refrescos embotellados o de vidrio.",
          count: 1,
          price: 13,
          total: 13,
          category: "Bebida"
        },
        {
          photo: require("@/assets/img/agua.jpg"),
          name: "Agua fresca",
          description: "Aguas frescas de distintos sabores.",
          count: 1,
          price: 12,
          total: 12,
          category: "Bebida"
        },
        {
          photo: require("@/assets/img/pan.jpg"),
          name: "Pan huasteco",
          description: "Conchas, cuernos, bisquetes, polvorones, etc.",
          count: 1,
          price: 4,
          total: 4,
          category: "Postre"
        },
        {
          photo: require("@/assets/img/empanadas.jpg"),
          name: "Empanadas",
          description: "Empanadas rellenas de queso, piña o cajeta.",
          count: 1,
          price: 4,
          total: 4,
          category: "Postre"
        },
        {
          photo: require("@/assets/img/pay.jpg"),
          name: "Pay de queso",
          description: "Tarta de queso philadelphia.",
          count: 1,
          price: 15,
          total: 15,
          category: "Postre"
        }
      ],
      productsBase:[
        {
          photo: require("@/assets/img/enchiladas.jpg"),
          name: "Enchiladas huastecas",
          description: "Orden de 5 tortillas bañadas en salsa verde acompañadas de verdura, cecina y frijoles refritos.",
          count: 1,
          price: 70,
          total: 70,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/zacahuil.jpg"),
          name: "Zacahuil",
          description: "Platillo estilo tamal revuelto con diferentes chiles y carne de cerdo o pollo para dar color y sabor, se acompaña con limon y chiles en vinagre.",
          count: 1,
          price: 40,
          total: 40,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/gorditas.jpeg"),
          name: "Gorditas",
          description: "Orden de 4 pzs. Tortillas infladas y rellenas de diferentes guisos.",
          count: 1,
          price: 35,
          total: 35,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/bocoles.jpg"),
          name: "Bocoles",
          description: "Orden de 4 pzs. Platillo a base de maza y manteca de puerco, se abren por la mitad y se rellenan de diferentes guisos.",
          count: 1,
          price: 40,
          total: 40,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/migada.jpg"),
          name: "Migada",
          description: "Tortilla gruesa grande pellizcada cubierta con diferentes guisos y verdura.",
          count: 1,
          price: 50,
          total: 50,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/sopes.jpg"),
          name: "Sopes",
          description: "Orden de 4 pzs. Tortilla gruesa pequeña pellizcada cubierta con diferentes guisos y verdura.",
          count: 1,
          price: 40,
          total: 40,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/menudo.jpg"),
          name: "Menudo",
          description: "Panzita y patitas de res en caldo con chile rojo.",
          count: 1,
          price: 70,
          total: 70,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/pozole.jpg"),
          name: "Pozole",
          description: "Carne de puerco con maiz en caldo y guisado con chile rojo.",
          count: 1,
          price: 60,
          total: 60,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/rojos.jpg"),
          name: "Tacos rojos",
          description: "Orden de 4 pzs. Tortillas fritas en salsa roja y rellenas de carne, papa o frijoles. Se acompaña con lechuga, tomate, queso y chiles en vinagre.",
          count: 1,
          price: 35,
          total: 35,
          category: "Comida"
        },
        {
          photo: require("@/assets/img/cafe.jpeg"),
          name: "Café",
          description: "Café de olla preparado con canela.",
          count: 1,
          price: 12,
          total: 12,
          category: "Bebida"
        },
        {
          photo: require("@/assets/img/refresco.jpg"),
          name: "Refresco",
          description: "Refrescos embotellados o de vidrio.",
          count: 1,
          price: 13,
          total: 13,
          category: "Bebida"
        },
        {
          photo: require("@/assets/img/agua.jpg"),
          name: "Agua fresca",
          description: "Aguas frescas de distintos sabores.",
          count: 1,
          price: 12,
          total: 12,
          category: "Bebida"
        },
        {
          photo: require("@/assets/img/pan.jpg"),
          name: "Pan huasteco",
          description: "Conchas, cuernos, bisquetes, polvorones, etc.",
          count: 1,
          price: 4,
          total: 4,
          category: "Postre"
        },
        {
          photo: require("@/assets/img/empanadas.jpg"),
          name: "Empanadas",
          description: "Empanadas rellenas de queso, piña o cajeta.",
          count: 1,
          price: 4,
          total: 4,
          category: "Postre"
        },
        {
          photo: require("@/assets/img/pay.jpg"),
          name: "Pay de queso",
          description: "Tarta de queso philadelphia.",
          count: 1,
          price: 15,
          total: 15,
          category: "Postre"
        }
      ],
      order:[],
      shopCar: 0,
      userState: {
        name: "admin",
        pass: "admin"
      },
      logged: false
    }),
    mutations: {
      // incrementa la cantidad en la vista de la tabla
      increaseCount(state, name){
        for(let i of state.products){
          if(i.name === name){
            i.count++;
            i.total += i.price;
          }
        }
      },
      // reduce la cantidad en la vista de la tabla
      reduceCount(state, name){
        for(let i of state.products){
          if(i.name === name){
            i.count--;
            i.total -= i.price;
          }
        }
      },
      // agrega la cantidad correcta al carrito
      addToShopCar(state, item){
        state.shopCar += item.count;
        state.order.push(item);
      },
      // incrementa la cantidad en la vista de la orden
      increaseProductOrder(state, name){
        for(let i of state.order){
          if(i.name === name){
            i.count++;
            i.total += i.price
          }
        }
      },
      // reduce la cantidad en la vista de la orden
      reduceProductOrder(state, name){
        for(let i of state.order){
          if(i.name === name){
            i.count--;
            i.total -= i.price
          }
        }
      },
      // elimina el producto en la vista de la orden
      cancelProduct(state, name){
        state.order = state.order.filter(product => product.name !== name);
        if(state.order.length === 0){
          state.shopCar = 0;
          state.products = state.productsBase;
          this.$router.push("/");
        }
      },
      // pago correcto y regresa al menu
      paySuccess(state){
        state.shopCar = 0;
        state.products = state.productsBase;
        this.$router.push("/");
      },
      // cambia el valor del logged
      changeLogged(state, value){
        state.logged = value;
      },
      // actualiza los datos del producto
      updateProduct(state, product){
        for(let i of state.products){
          if(i.name === product.name){
            i.name = product.name;
            i.price = parseInt(product.price);
            i.total = parseInt(product.price);
            i.description = product.description;
          }
        }
      }
    },
    getters:{
      // regresa el precio total a pagar
      totalPrice(state){
        let total = 0;
        for(let i of state.order){
          total += i.total;
        }
        return total;
      }
    }
  })
}

export default createStore
